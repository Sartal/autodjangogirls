FROM python:slim
WORKDIR /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
COPY . /app
RUN chmod +x init.sh
ENTRYPOINT /app/init.sh
